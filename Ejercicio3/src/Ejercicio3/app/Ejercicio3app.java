package Ejercicio3.app;

public class Ejercicio3app {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//DECLARACIÓN DE VARIABLES
		int x = 10, y = 55;
		double n = 30.3, m = 40.5;
		
		//VALOR DE CADA VARIABLE
		System.out.println("VALOR DE LA VARIABLE X: " + x);
		
		System.out.println("VALOR DE LA VARIABLE Y: " + y);
		
		System.out.println("VALOR DE LA VARIABLE N: " + n);
		
		System.out.println("VALOR DE LA VARIABLE M: " + m);
		
		//ESPACIADOR
		System.out.println();
		
		//SUMA DE X+Y
		System.out.println("SUMA DE X + Y: " + (x+y));
		
		//DIFERENCIA DE X-Y
		System.out.println("DIFERENCIA DE X - Y: " + (x-y));
		
		//PRODUCTO DE X*Y
		System.out.println("PRODUCTO DE X * Y: " + (x*y));
		
		//COCIENTE DE X/Y
		System.out.println("COCIENTE DE X / Y: " + (x/y));
		
		//RESTO DE X%Y
		System.out.println("RESTO DE X % Y: " + (x%y));
		
		//ESPACIADOR
		System.out.println();
		
		//SUMA DE N+M
		System.out.println("SUMA DE N + M: " + (n+m));
		
		//DIFERENCIA DE N-M
		System.out.println("DIFERENCIA DE N - M: " + (n-m));
		
		//PRODUCTO DE N*M
		System.out.println("PRODUCTO DE N * M: " + (n*m));
		
		//COCIENTE DE N/M
		System.out.println("COCIENTE DE N / M: " + (n/m));
		
		//RESTO DE X%Y
		System.out.println("RESTO DE X % Y: " + (x%y));

		//ESPACIADOR
		System.out.println();
		
		//DOBLES DE CADA VARIABLE
		System.out.println("DOBLE DE X: " + (x*2));
		
		System.out.println("DOBLE DE Y: " + (y*2));
		
		System.out.println("DOBLE DE N: " + (n*2));
		
		System.out.println("DOBLE DE M: " + (m*2));
		
		//SUMA DE TODAS LAS VARIABLES
		System.out.println("SUMA DE TODAS LAS VARIABLES: " + (x+y+n+m));
		
		//PRODUCTO DE TODAS LAS VARIABLES
		System.out.println("PRODUCTO DE TODAS LAS VARIABLES: " + (x*y*n*m));
		
	}

}
